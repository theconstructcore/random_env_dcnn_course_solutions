cd /home/user/ai_ws
virtualenv --system-site-packages -p python3 ./dnn_venv
source ./dnn_venv/bin/activate
pip install --upgrade pip
pip install --upgrade tensorflow
python -c "import tensorflow as tf; print(tf.__version__)"
pip install keras
python -c 'import keras; print(keras.__version__)'
pip install pydot
pip3 install imgaug
pip install opencv-python
pip install gym

# Things to do with opencv bridge compilation for python 3
# To avoid error in further compilation stating: ImportError: No module named 'em', Error
pip uninstall em
pip install empy

catkin config -DPYTHON_EXECUTABLE=/usr/bin/python3 -DPYTHON_INCLUDE_DIR=/usr/include/python3.5m -DPYTHON_LIBRARY=/usr/lib/x86_64-linux-gnu/libpython3.5m.so
# Instruct catkin to install built packages into install place. It is $CATKIN_WORKSPACE/install folder
catkin config --install
# Clone cv_bridge src
git clone https://github.com/ros-perception/vision_opencv.git src/vision_opencv
# Find version of cv_bridge in your repository
apt-cache show ros-kinetic-cv-bridge | grep Version
    Version: 1.12.8-0xenial-20180416-143935-0800
# Checkout right version in git repo. In our case it is 1.12.8
cd src/vision_opencv/
git checkout 1.12.8
### CHANGE https://stackoverflow.com/questions/49221565/unable-to-use-cv-bridge-with-ros-kinetic-and-python3
### src/vision_opencv/cv_bridge/CMakeLists.txt
### find_package(Boost REQUIRED python3)-->to-->find_package(Boost REQUIRED python-py35)
sed -i -- 's/Boost REQUIRED python3/Boost REQUIRED python-py35/g' ./cv_bridge/CMakeLists.txt

cd ../../
catkin_make

cd /home/user/ai_ws/src
git clone https://github.com/ros-simulation/gazebo_ros_pkgs.git
rm -rf gazebo_ros_pkgs/gazebo_plugins gazebo_ros_pkgs/gazebo_ros_control
cd /home/user/ai_ws
catkin_make

cd /home/user/ai_ws/src
git clone https://github.com/ros/ros_comm_msgs.git
cd /home/user/ai_ws
catkin_make


cd /home/user/ai_ws/src
git clone https://github.com/ros/common_msgs.git
mv common_msgs/visualization_msgs/ ./
rm -rf common_msgs
cd /home/user/ai_ws
catkin_make






