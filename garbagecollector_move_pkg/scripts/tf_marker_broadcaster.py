#! /usr/bin/env python
import rospy
import time
import tf
import sys
from visualization_msgs.msg import Marker


class TfMarkerBroadcaster(object):
    
    def __init__(self, model_name, pred_marker_topic):
        
        self.br = tf.TransformBroadcaster()
        self.model_name = model_name
        self._pred_marker_topic = pred_marker_topic
        self._check_rvizmarker_ready()
        rospy.Subscriber(self._pred_marker_topic, Marker, self.marker_callback)

    def handle_turtle_pose(self,pose_msg):
        
        rospy.logdebug("handle_turtle_pose")
        
        
        
        self.br.sendTransform((pose_msg.position.x,pose_msg.position.y,pose_msg.position.z),
                         (pose_msg.orientation.x,pose_msg.orientation.y,pose_msg.orientation.z,pose_msg.orientation.w),
                         rospy.Time.now(),
                         self.model_name,
                         "/world")
                         
        rospy.logdebug("handle_turtle_pose...DONE")
    
    def _check_rvizmarker_ready(self):
        predicted_position_marker = None
        while predicted_position_marker is None and not rospy.is_shutdown():
            try:
                predicted_position_marker = rospy.wait_for_message(self._pred_marker_topic, Marker, timeout=1.0)
                rospy.logdebug( str(self._pred_marker_topic)+"READY=>"+str(predicted_position_marker))
            except:
                rospy.logerr(str(self._pred_marker_topic)+" not ready yet, retrying....")
        
        rospy.logdebug("rvizmarker READY!")
        
    def marker_callback(self,msg):
        self.handle_turtle_pose(msg.pose)
        
    def publisher_of_tf(self):
            
        rospy.logdebug("Starting publisher_of_tf...")
        rospy.spin()        
    

if __name__ == '__main__':
    rospy.init_node('garbagecollector_tf_broadcaster_node', anonymous=True, log_level=rospy.WARN)
    
    if len(sys.argv) < 4:
        print("usage: tf_broadcaster.py model_name pred_marker_topic")
    else:
        model_name = sys.argv[1]
        pred_marker_topic = sys.argv[2]
    
        tf_marker_object = TfMarkerBroadcaster( model_name,
                                                pred_marker_topic)
        tf_marker_object.publisher_of_tf()