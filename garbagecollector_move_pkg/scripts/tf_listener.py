#!/usr/bin/env python
import sys
import rospy
import math
import tf
import geometry_msgs.msg
from std_srvs.srv import Empty, EmptyRequest

if __name__ == '__main__':
    rospy.init_node('tf_listener_turtle', anonymous=True, log_level=rospy.WARN)

    listener = tf.TransformListener()

    if len(sys.argv) < 5:
        print("usage: turtle_tf_listener.py follower_model_name model_to_be_followed_name cmd_vel_namespace distance_to_object")
    else:
        follower_model_name = sys.argv[1]
        model_to_be_followed_name = sys.argv[2]
        cmd_vel_namespace = sys.argv[3]
        distance_to_object = float(sys.argv[4])
        
        turtle_vel = rospy.Publisher(cmd_vel_namespace+'/cmd_vel', geometry_msgs.msg.Twist,queue_size=1)
    
        rate = rospy.Rate(30.0)
        ctrl_c = False
        
        follower_model_frame = "/"+follower_model_name
        model_to_be_followed_frame = "/"+model_to_be_followed_name
        
        def shutdownhook():
            # works better than the rospy.is_shut_down()
            global ctrl_c
            print "shutdown time! Stop the robot"
            cmd = geometry_msgs.msg.Twist()
            cmd.linear.x = 0.0
            cmd.angular.z = 0.0
            turtle_vel.publish(cmd)
            ctrl_c = True

        rospy.on_shutdown(shutdownhook)
        
        rospy.wait_for_service('/garbagecollector_pick')
        garbagecollector_pick_srv = rospy.ServiceProxy('/garbagecollector_pick', Empty)
        pick_request = EmptyRequest()
        
        while not ctrl_c:
            try:
                (trans,rot) = listener.lookupTransform(follower_model_frame, model_to_be_followed_frame, rospy.Time(0))
                
            except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
                continue
    
            delta_angular = math.atan2(trans[1], trans[0])
            delta_linear = math.sqrt(trans[0] ** 2 + trans[1] ** 2)
        
            if abs(round(delta_linear,2)) == distance_to_object:
                linear = 0.0
                rospy.logwarn("In Optimum Picking Distance")
            elif abs(round(delta_linear,2)) < distance_to_object:
                linear = -0.1 * delta_linear
                rospy.logwarn("TOO Close to Object!")
            else:
                linear = 0.1 * delta_linear
                rospy.logwarn("TOO Faraway to Object!")
            
            angular = -4.0 * delta_angular
            
            # print("[linear,angular]=["+str(delta_linear)+","+str(delta_angular)+"]")
            cmd = geometry_msgs.msg.Twist()
            cmd.linear.x = linear
            cmd.angular.z = angular
            turtle_vel.publish(cmd)
            
            if linear == 0.0:
                # Lets pick the object
                rospy.logwarn("Ready to pick Object...Lets GO!")
                garbagecollector_pick_srv(pick_request)
                rospy.logwarn("Done picking Object...")
    
            rate.sleep()