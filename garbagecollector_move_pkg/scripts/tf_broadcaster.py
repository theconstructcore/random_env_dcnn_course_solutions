#! /usr/bin/env python
import rospy
import time
import tf
import sys
from garbagecollector_move_pkg.get_model_gazebo_pose import GazeboModel


def handle_turtle_pose(pose_msg, robot_name):
    br = tf.TransformBroadcaster()
    
    br.sendTransform((pose_msg.position.x,pose_msg.position.y,pose_msg.position.z),
                     (pose_msg.orientation.x,pose_msg.orientation.y,pose_msg.orientation.z,pose_msg.orientation.w),
                     rospy.Time.now(),
                     robot_name,
                     "/world")

def publisher_of_tf():
    
    rospy.init_node('garbagecollector_tf_broadcaster_node', anonymous=True)
    
    
    if len(sys.argv) < 2:
        print("usage: tf_broadcaster.py list_of_models_to_pub_tf")
    else:
        list_of_models_to_pub_tf = sys.argv[1]
        robot_name_list = list_of_models_to_pub_tf.split(",")
    
        gazebo_model_object = GazeboModel(robot_name_list)
        
        
        for robot_name in robot_name_list:
            pose_now = gazebo_model_object.get_model_pose(robot_name)
        
        # Leave enough time to be sure the Gazebo Model logs have finished
        rospy.sleep(1)
        rospy.loginfo("Ready..Starting to Publish TF data now...")
        
        rate = rospy.Rate(5) # 5hz
        while not rospy.is_shutdown():
            for robot_name in robot_name_list:
                pose_now = gazebo_model_object.get_model_pose(robot_name)
                if not pose_now:
                    print "The Pose is not yet"+str(robot_name)+" available...Please try again later"
                else:
                    handle_turtle_pose(pose_now, robot_name)
            rate.sleep()
    

if __name__ == '__main__':
    try:
        publisher_of_tf()
    except rospy.ROSInterruptException:
        pass