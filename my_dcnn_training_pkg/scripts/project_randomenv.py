#!/usr/bin/env python
import rospy
import time
import random
import gym
import math
import rospkg
import os
import copy
from geometry_msgs.msg import Pose
from my_randomgazebomanager_pkg.rviz_markers import MarkerBasics
from sensor_msgs.msg import Image
from get_model_gazebo_pose import GazeboModel
from std_srvs.srv import Empty, EmptyRequest

# Dont put anything ros related after this import because it removes ROS from imports to 
# import the cv2 installed and not the ROS version
from rgb_camera_python3 import RGBCamera
import sys
try:
    sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist-packages')
except ValueError:
    ImportError
import cv2
import numpy as np

from train_model import create_model
from keras.applications.mobilenetv2 import preprocess_input

import xml.etree.ElementTree as ET


class RobotDCNN():
    def __init__(self, weight_file_name, image_size, ALPHA, number_of_elements_to_be_output, model_to_track_name):
        
        
        self._image_size = image_size
        self._number_of_elements_to_be_output = number_of_elements_to_be_output
        
        self.init_rviz_markers()
        
        # Init camera RGB object
        self.rgb_camera_object = RGBCamera("/dynamic_objects/camera/raw_image")
        
        # This are the models that we will generate information about.
        self.model_to_track_name = model_to_track_name
        
        model_to_track_list = [self.model_to_track_name]
        self.gz_model_obj = GazeboModel(model_to_track_list)
        
        # We start the model in Keras
        self.model = create_model(self._image_size, ALPHA, self._number_of_elements_to_be_output)
        
        rospack = rospkg.RosPack()
        # get the file path for rospy_tutorials
        path_to_package = rospack.get_path('my_dcnn_training_pkg')
        models_weight_checkpoints_folder = os.path.join(path_to_package, "bk")
        model_file_path = os.path.join(models_weight_checkpoints_folder, weight_file_name)
        
        print (model_file_path)
        
        self.model.load_weights(model_file_path)
        
        self.testing_unscaled_img_folder = os.path.join(path_to_package, "testing/dataset_gen/images")
        self.testing_unscaled_anotations_folder = os.path.join(path_to_package, "testing/dataset_gen_annotations")
        
        
        # We reset the world to reset garbage collector pose
        print("Waiting for Service to Reset Sim....")
        rospy.wait_for_service('/gazebo/reset_world')
        print("Starting Service to Reset Sim....")
        self.reset_sim_service_call = rospy.ServiceProxy('/gazebo/reset_world', Empty)
        self.reset_sim_request = EmptyRequest()
        print("Starting Service to Reset Sim....DONE")
        
        # We change the environent to a random state
        print("Waiting for Service to Change World Randomly....")
        rospy.wait_for_service('/dynamic_world_service')
        print("Starting Service to Change World Randomly....")
        self.dynamic_world_service_call = rospy.ServiceProxy('/dynamic_world_service', Empty)
        self.change_env_request = EmptyRequest()
        self.dynamic_world_service_call(self.change_env_request)
        print("Starting Service to Change World Randomly....DONE")
        
        

    def init_rviz_markers(self):
        """
        We initialise the markers used to visualise the predictions vs the 
        real data.
        """
        # We initialise the Markers for Center Of Mass estimation and the Object real position

        marker_type = "mesh"
        namespace = "real_position"
        mesh_package_path = "models_spawn_library_pkg/models/banana/meshes/banana.dae"
    
        self.spam_real_position = MarkerBasics(type=marker_type,
                                            namespace=namespace,
                                            index=0,
                                            red=0.0,
                                            green=0.0,
                                            blue=0.0,
                                            alfa=0.0,
                                            scale=[1.0, 1.0, 1.0],
                                            mesh_package_path=mesh_package_path)
    
        marker_type = "sphere"
        mesh_package_path = ""
        namespace = "com_prediction"
        self.com_prediction_marker = MarkerBasics(type=marker_type,
                                            namespace=namespace,
                                            index=0,
                                            red=1.0,
                                            green=0.0,
                                            blue=0.0,
                                            alfa=1.0,
                                            scale=[0.1, 0.1, 0.1],
                                            mesh_package_path=mesh_package_path)
                                            

    
    def publish_markers_new_data(self,pred, reality):
        
        spam_real_pose = Pose()
        spam_real_pose.position.x = reality[0]
        spam_real_pose.position.y = reality[1]
        spam_real_pose.position.z = reality[2]
        spam_real_pose.orientation.w = 1.0

        com_prediction_pose = Pose()
        com_prediction_pose.position.x = pred[0]
        com_prediction_pose.position.y = pred[1]
        com_prediction_pose.position.z = pred[2]
        com_prediction_pose.orientation.w = 1.0


        self.spam_real_position.publish_marker(spam_real_pose)
        self.com_prediction_marker.publish_marker(com_prediction_pose)
        
    def predict_image(self,model,cv2_img=None, path=None):
        
        if cv2_img.all() != None:
            im = cv2_img
        else:
            if path != None:
                im = cv2.imread(path)
            else:
                print ("Predict Image had no path image or image CV2")
                return None
        
        if im.shape[0] != self._image_size:
            im = cv2.resize(im, (self._image_size, self._image_size))
            """
            self.rgb_camera_object.display_image(   image_display=im,
                                                    life_time_ms=50,
                                                    name="ResizedCAM"
                                                )
            """
    
        image = np.array(im, dtype='f')
        image = preprocess_input(image)
        
        
        self.rgb_camera_object.display_image(   image_display=image,
                                                    life_time_ms=50,
                                                    name="ImagePredict"
                                                )
    
        prediction = model.predict(x=np.array([image]))[0]
    
        return prediction
    
    def get_xyz_from_world(self, model_name):
        """
        Retrieves the position of an object from the world
        """
        pose_now = self.gz_model_obj.get_model_pose(model_name)
        
        XYZ = [pose_now.position.x,pose_now.position.y,pose_now.position.z]
        
        return XYZ
    
    
    
    def start_camera_rgb_prediction_continuous(self, pred_freq= 10.0, wait_reset_period=3.0, z_value = 0.0):
        
        # We reset The simulation Once
        print ("Waiting for Reset World to settle=>")
        self.reset_sim_service_call(self.reset_sim_request)
        wait_reset_period = 3.0
        rospy.sleep(wait_reset_period)
        print ("Waiting for Reset World to settle=>DONE")
        
        self.dynamic_world_service_call(self.change_env_request)
        print ("Waiting for Randomise Env to settle=>")
        wait_reset_period = 3.0
        rospy.sleep(wait_reset_period)
        print ("Waiting for Randomise Env to settle==>DONE")
        
        
        rate = rospy.Rate(pred_freq)
        life_time_ms = int((1.0/ pred_freq)*1000)
        while not rospy.is_shutdown():
        
            cv2_img = self.rgb_camera_object.get_latest_image()
            pred = self.predict_image(self.model,cv2_img)
            # We add Z value because we dont predict it
            pred_mod = [pred[0],pred[1],z_value]
            
            reality = self.get_xyz_from_world(self.model_to_track_name)
            
            print ("Class Prediction XYZ=>" + str(pred_mod))
            print ("Class Reality SIMULATION=>" + str(reality))
            self.publish_markers_new_data(pred_mod, reality)

        rospy.loginfo("Start Prediction DONE...")
    
    
        
if __name__ == '__main__':
    rospy.init_node('project_randomenv_node', anonymous=True, log_level=rospy.INFO)
    
    
    if len(sys.argv) < 6:
        rospy.logfatal("usage: project_randomenv.py weight_file_name image_size ALPHA number_of_elements_to_be_output model_to_track_name")
    else:
        
        rospy.logwarn(str(sys.argv))
        
        weight_file_name = sys.argv[1]
        image_size = int(sys.argv[2])
        ALPHA = float(sys.argv[3])
        number_of_elements_to_be_output = int(sys.argv[4])
        model_to_track_name = sys.argv[5]
        
        rospy.logwarn("weight_file_name:"+str(weight_file_name))
        rospy.logwarn("image_size:"+str(image_size))
        rospy.logwarn("ALPHA:"+str(ALPHA))
        rospy.logwarn("number_of_elements_to_be_output:"+str(number_of_elements_to_be_output))
        rospy.logwarn("model_to_track_name:"+str(model_to_track_name))
    
        agent = RobotDCNN(  weight_file_name,
                            image_size,
                            ALPHA,
                            number_of_elements_to_be_output,
                            model_to_track_name)
                                    
        agent.start_camera_rgb_prediction_continuous()
